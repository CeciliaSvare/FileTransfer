Sending Files to Another Device 
============================

This lesson will show you how to design your app to send large files to another device using Android Beam file transfer. Easy to implement.

Cecilia Svare - Executive at http://applianceauthority.org/coffee-makers/


Cryptographic software is subject to the US government export control and
economic sanctions laws (“US export laws”) including the US Department of
Commerce Bureau of Industry and Security’s (“BIS”) Export Administration
Regulations (“EAR”, 15 CFR 730 et seq., http://www.bis.doc.gov/).  You may
also be subject to US export laws, including the requirements of license
exception TSU in accordance with part 740.13(e) of the EAR.  Software
and/or technical data subject to the US export laws may not be directly or
indirectly exported, reexported, transferred, or released (“exported”) to
US embargoed or sanctioned destinations currently including Cuba, Iran,
North Korea, Sudan, or Syria, but any amendments to this list shall apply.
In addition, software and/or technical data may not be exported to any
entity barred by the US government from participating in export activities.
Denied persons or entities include those listed on BIS’s Denied Persons and
Entities Lists, and the US Department of Treasury’s Office of Foreign
Assets Control’s Specially Designated Nationals List.  The country in which
you are currently located may have restrictions on the import, possession,
use of encryption software. You are responsible for compliance with the
laws where You are located.


public class MainActivity extends Activity {
    ...
    NfcAdapter mNfcAdapter;
    // Flag to indicate that Android Beam is available
    boolean mAndroidBeamAvailable  = false;
    ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
        // NFC isn't available on the device
        if (!PackageManager.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            /*
             * Disable NFC features here.
             * For example, disable menu items or buttons that activate
             * NFC-related features
             */
            ...
        // Android Beam file transfer isn't supported
        } else if (Build.VERSION.SDK_INT <
                Build.VERSION_CODES.JELLY_BEAN_MR1) {
            // If Android Beam isn't available, don't continue.
            mAndroidBeamAvailable = false;
            /*
             * Disable Android Beam file transfer features here.
             */
            ...
        // Android Beam file transfer is available, continue
        } else {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        ...
        }
    }
    ...
}
